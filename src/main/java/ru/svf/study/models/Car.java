package ru.svf.study.models;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "car")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "make")
    @NotEmpty(message = "Make should be present")
    private String make;
    @Column(name = "model")
    @NotEmpty(message = "Model should be present")
    private String model;
    @Column(name = "year_of_production")
    @Min(value = 1921,message = "year of production should be from 1921")
    private int yearOfProduction;
}
