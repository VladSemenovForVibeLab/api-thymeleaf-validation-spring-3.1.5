package ru.svf.study.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.svf.study.models.Car;
import ru.svf.study.repositories.CarRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CarService {
    private final CarRepository carRepository;
    public List<Car> getAll(){
     return carRepository.findAll();
    }
    public Car getById(int id){
        return carRepository
                .findById(id)
                .stream()
                .findAny()
                .orElse(null);
    }
    public void save(Car car){
        carRepository.save(car);
    }
    public void edit(Car car, int id){
        car.setId(id);
        carRepository.save(car);
    }
    public void delete(int id){
        carRepository.deleteById(id);
    }
}
