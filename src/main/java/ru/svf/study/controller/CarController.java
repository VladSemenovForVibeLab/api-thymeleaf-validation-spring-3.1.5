package ru.svf.study.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.Banner;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.svf.study.models.Car;
import ru.svf.study.services.CarService;

@Controller
@RequestMapping("/cars")
@RequiredArgsConstructor
public class CarController {
    private final CarService carService;
    @GetMapping()
    public String getAll(Model model){
        model.addAttribute("cars",carService.getAll());
        return "cars";
    }
    @GetMapping("/{id}")
    public String getById(Model model,
                          @PathVariable("id") int id){
        model.addAttribute("car",carService.getById(id));
        return "car";
    }
    @GetMapping("/new")
    public String newCarPage(@ModelAttribute("car")Car car){
         return "new";
    }
    @PostMapping()
    public String createCar(@ModelAttribute("car") @Valid Car car,
                            BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return "new";
        }
        carService.save(car);
        return "redirect:/cars";
    }
    @GetMapping("/edit/{id}")
    public String editCarPage(Model model,
                              @PathVariable("id") int id){
        model.addAttribute("car",carService.getById(id));
        return "edit";
    }
    @PatchMapping("/{id}")
    public String updateCar(@PathVariable("id") int id,
                            @ModelAttribute("car") @Valid Car car,
                            BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return "edit";
        }
        carService.edit(car,id);
        return "redirect:/cars";
    }
    @DeleteMapping("/{id}")
    public String delete(@PathVariable("id")int id){
        carService.delete(id);
        return "redirect:/cars";
    }
}
