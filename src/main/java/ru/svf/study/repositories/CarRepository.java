package ru.svf.study.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.svf.study.models.Car;

@Repository
public interface CarRepository extends JpaRepository<Car,Integer> {
}
