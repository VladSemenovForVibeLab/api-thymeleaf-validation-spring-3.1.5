create table car
(
    id int generated by default as identity primary key,
    make varchar not null,
    model varchar not null ,
    year_of_production int check ( year_of_production>1920 ) not null
)