# Приложение для управления и учета машинами

Данное приложение представляет собой систему для управления и учета машин, разработанную с использованием Spring Boot, Lombok, Maven, Java 17, Thymeleaf, PostgreSQL, Spring Data JPA и Starter Validation.

## Установка и настройка

Для установки данного приложения необходимо склонировать проект из репозитория. Убедитесь, что у вас установлен Maven, и выполните сборку проекта с помощью команды:

```
mvn clean install
```

После успешной сборки вы можете запустить приложение

## Модель машины

Для создания объектов машины в приложении используется следующая модель:

```
@Entity
@Table(name = "car")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Car {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name = "id")
private int id;
@Column(name = "make")
@NotEmpty(message = "Make should be present")
private String make;
@Column(name = "model")
@NotEmpty(message = "Model should be present")
private String model;
@Column(name = "year_of_production")
@Min(value = 1921, message = "Year of production should be from 1921")
private int yearOfProduction;
}
```

## Использование

Приложение предоставляет доступ к различным эндпоинтам для управления машинами, таким как добавление, удаление, редактирование и просмотр всех машин.

```
@Controller
@RequestMapping("/cars")
@RequiredArgsConstructor
public class CarController {
private final CarService carService;
    @GetMapping()
    public String getAll(Model model){
        model.addAttribute("cars",carService.getAll());
        return "cars";
    }
    @GetMapping("/{id}")
    public String getById(Model model,
                          @PathVariable("id") int id){
        model.addAttribute("car",carService.getById(id));
        return "car";
    }
    @GetMapping("/new")
    public String newCarPage(@ModelAttribute("car")Car car){
         return "new";
    }
    @PostMapping()
    public String createCar(@ModelAttribute("car") @Valid Car car,
                            BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return "new";
        }
        carService.save(car);
        return "redirect:/cars";
    }
    @GetMapping("/edit/{id}")
    public String editCarPage(Model model,
                              @PathVariable("id") int id){
        model.addAttribute("car",carService.getById(id));
        return "edit";
    }
    @PatchMapping("/{id}")
    public String updateCar(@PathVariable("id") int id,
                            @ModelAttribute("car") @Valid Car car,
                            BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return "edit";
        }
        carService.edit(car,id);
        return "redirect:/cars";
    }
    @DeleteMapping("/{id}")
    public String delete(@PathVariable("id")int id){
        carService.delete(id);
        return "redirect:/cars";
    }
}
```

## Технологии

Проект разработан с использованием следующих технологий:
- Spring Boot
- Lombok
- Maven
- Java 17
- Thymeleaf
- PostgreSQL
- Spring Data JPA
- Validation Starter
- Spring Web

## Благодарности

Благодарю за интерес к приложению!